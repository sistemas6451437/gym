USE w210811_energym
GO

/****** Object:  StoredProcedure [dbo].[AfiliadosEnMora]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AfiliadosEnMora] AS
BEGIN
    SELECT 
        A.AfiliadoId,
        A.Nombre,
        A.Apellido,
        A.Email,
        A.Telefono,
        S.Descripcion AS Suscripcion,
        S.Monto AS MontoSuscripcion,
        (S.Monto - (S.Monto * ISNULL((SELECT TOP 1 Descuento FROM AsignacionPromocionesAfiliados APA INNER JOIN Promociones P ON APA.PromocionId = P.PromocionId
        WHERE APA.AfiliadoId = A.AfiliadoId AND P.Activa = 1 ORDER BY P.FechaCaducidad DESC), 0) / 100)) AS MontoConDescuento,
        (SELECT TOP 1 Monto FROM Cobro C WHERE C.AfiliadoId = A.AfiliadoId AND MONTH(C.Fecha) = MONTH(GETDATE()) 
        AND YEAR(C.Fecha) = YEAR(GETDATE())) AS MontoPagado,
        CASE 
            WHEN S.Monto - (S.Monto * ISNULL((SELECT TOP 1 Descuento FROM AsignacionPromocionesAfiliados APA INNER JOIN Promociones P ON APA.PromocionId = P.PromocionId
            WHERE APA.AfiliadoId = A.AfiliadoId AND P.Activa = 1 ORDER BY P.FechaCaducidad DESC), 0) / 100) > ISNULL((SELECT TOP 1 Monto FROM Cobro C WHERE C.AfiliadoId = A.AfiliadoId AND MONTH(C.Fecha) = MONTH(GETDATE()) 
            AND YEAR(C.Fecha) = YEAR(GETDATE())), 0) THEN 'En Mora'
            ELSE 'Al Dia'
        END AS Estado
    FROM 
        Afiliados A 
    INNER JOIN 
        Suscripcion S ON A.SuscripcionId = S.SuscripcionId
    WHERE 
        A.Activo = 1 
    ORDER BY 
        A.Apellido, 
        A.Nombre;
END;
GO
/****** Object:  StoredProcedure [dbo].[CrearCuotas]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CrearCuotas] AS
BEGIN
	INSERT INTO dbo.Cuotas (AfiliadoId, Monto, FechaCuota, Estado, MontoPendiente)
	SELECT A.AfiliadoId, S.Monto, GETDATE(), 'ADEUDADO', S.Monto
	FROM dbo.Afiliados A
	JOIN dbo.Suscripcion S
	ON A.SuscripcionId = S.SuscripcionId
	WHERE DAY(A.FechaAlta) = DAY(GETDATE())
END
GO
/****** Object:  StoredProcedure [dbo].[sp_AnularCobro]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_AnularCobro]
    @CobroId INT
AS
BEGIN
    -- Verificar que el cobro fue hecho en el mismo mes
    IF (SELECT MONTH(Fecha) FROM dbo.Cobro WHERE CobroId = @CobroId) <> MONTH(GETDATE())
    BEGIN
        -- Retornar un error
        RAISERROR ('Los cobros sólo pueden ser anulados en el mismo mes en que fueron realizados', 16, 1);
        RETURN;
    END
	
    -- Eliminar los registros de exceso de pago generados por el cobro
    DELETE FROM dbo.ExcesoPago WHERE CobroId = @CobroId

    -- Obtener los detalles de las cuotas que se pagaron con el cobro
    DECLARE @CuotaId INT, @MontoPendiente DECIMAL(10,2), @MontoPago DECIMAL(10,2), @FechaPago DATETIME
    DECLARE CuotasCursor CURSOR FOR 
        SELECT CuotaId, MontoPago, FechaPago
		FROM dbo.CobrosCuotas 
		WHERE CobroId = @CobroId 
		ORDER BY FechaPago DESC

    -- Recorrer las cuotas en orden inverso al que fueron pagadas
    OPEN CuotasCursor
    FETCH NEXT FROM CuotasCursor INTO @CuotaId, @MontoPago, @FechaPago
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Incrementar el monto pendiente de la cuota
        UPDATE dbo.Cuotas 
		SET MontoPendiente = MontoPendiente + @MontoPago,
			Estado = 'ADEUDADO'
		WHERE CuotaId = @CuotaId

		--2023/08/05 => Agregamos el campo anulación al cobro cuotas.
		--UPDATE dbo.CobrosCuotas
		--SET FechaAnulacion = GETDATE()
		--WHERE CuotaId = @CuotaId

		INSERT INTO dbo.CobrosCuotasAnulados (CobroId, CuotaId, MontoPago, FechaPago, FechaAnulacion)
		VALUES (@CobroId, @CuotaId, @MontoPago, @FechaPago, GETDATE())

		DELETE FROM dbo.CobrosCuotas
		WHERE CuotaId = @CuotaId
		AND CobroId = @CobroId
        -- Continuar con la siguiente cuota
		FETCH NEXT FROM CuotasCursor INTO @CuotaId, @MontoPago, @FechaPago
    END
    CLOSE CuotasCursor
    DEALLOCATE CuotasCursor

    -- Eliminar el cobro
    --DELETE FROM dbo.Cobro WHERE CobroId = @CobroId
	    UPDATE dbo.Cobro SET FechaAnulacion = GETDATE() WHERE CobroId = @CobroId

END
GO
/****** Object:  StoredProcedure [dbo].[sp_CrearAfiliadoYGenerarCuota]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_CrearAfiliadoYGenerarCuota]
	@Nombre VARCHAR(50),
	@Apellido VARCHAR(50),
	@Telefono VARCHAR(100),
	@DNI VARCHAR(100),
	@Email VARCHAR(100),
	@SuscripcionId INT,
	@UsuarioId INT,
	@PromocionId INT = NULL
AS
BEGIN
	DECLARE @AfiliadoId INT;
	DECLARE @Monto DECIMAL(10,2);
	DECLARE @FechaAlta DATE = GETDATE();
	DECLARE @MontoDespuesPromocion DECIMAL(10,2);

	-- Insertamos el nuevo afiliado
	INSERT INTO dbo.Afiliados (Nombre, Apellido, Telefono, DNI, Email, FechaAlta, Activo, SuscripcionId, UsuarioId)
	VALUES (@Nombre, @Apellido, @Telefono, @DNI, @Email, @FechaAlta, 1, @SuscripcionId, @UsuarioId);

	-- Obtenemos el ID del afiliado que acabamos de insertar
	SET @AfiliadoId = SCOPE_IDENTITY();

	-- Si se proporcionó un ID de promoción, la asignamos al afiliado
	IF @PromocionId IS NOT NULL
		INSERT INTO dbo.AsignacionPromocionesAfiliados (AfiliadoId, PromocionId)
		VALUES (@AfiliadoId, @PromocionId);

	-- Obtenemos el monto de la suscripción
	SELECT @Monto = Monto FROM dbo.Suscripcion WHERE SuscripcionId = @SuscripcionId;

	-- Calculamos el monto después de aplicar la promoción si existe alguna activa para el afiliado
	SET @MontoDespuesPromocion = @Monto - (@Monto * ISNULL((SELECT TOP 1 Descuento FROM dbo.AsignacionPromocionesAfiliados APA INNER JOIN dbo.Promociones P ON APA.PromocionId = P.PromocionId
	WHERE APA.AfiliadoId = @AfiliadoId AND P.Activa = 1 ORDER BY P.FechaCaducidad DESC), 0) / 100);

	-- Insertamos una nueva cuota con el monto calculado
	INSERT INTO dbo.Cuotas (AfiliadoId, Monto, MontoPendiente, FechaCuota, Estado)
	VALUES (@AfiliadoId, @MontoDespuesPromocion, @MontoDespuesPromocion, GETDATE(), 'ADEUDADO');
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_CrearPromocion]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_CrearPromocion]
    @Nombre VARCHAR(50),
    @Descuento INT,
    @FechaCaducidad DATE,
    @UsuarioId INT
AS
BEGIN
    INSERT INTO dbo.Promociones (Nombre, Descuento, FechaCaducidad, Activa, FechaCreacion, UsuarioId)
    VALUES (@Nombre, @Descuento, @FechaCaducidad, 1, GETDATE(), @UsuarioId);
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_DesactivarAfiliado]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_DesactivarAfiliado]
    @AfiliadoId INT
AS
BEGIN
    -- Verificamos si el afiliado tiene cuotas pendientes
    IF EXISTS (
        SELECT 1 
        FROM dbo.Cuotas 
        WHERE AfiliadoId = @AfiliadoId AND Estado IN ('ADEUDADO', 'PARCIAL')
    )
    BEGIN
        RAISERROR('El afiliado tiene cuotas pendientes, no se puede desactivar', 16, 1);
        RETURN;
    END;

    UPDATE dbo.Afiliados
    SET Activo = 0
    WHERE AfiliadoId = @AfiliadoId;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_DesactivarPromocion]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_DesactivarPromocion]
    @PromocionId INT
AS
BEGIN
    UPDATE dbo.Promociones
    SET Activa = 0
    WHERE PromocionId = @PromocionId;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_DesactivarUsuario]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_DesactivarUsuario]
    @UsuarioId INT
AS
BEGIN
    UPDATE dbo.Usuarios
    SET Activo = 0
    WHERE UsuarioId = @UsuarioId;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_EstadoDeCuenta]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_EstadoDeCuenta] @AfiliadoId INT
AS
BEGIN
    SELECT 
		A.AfiliadoId,
		A.Nombre,
		A.Apellido,
		CASE 
			WHEN (
					SELECT ISNULL(SUM(MontoPendiente), 0) 
					FROM dbo.Cuotas 
					WHERE AfiliadoId = A.AfiliadoId 
					AND Estado IN ('ADEUDADO', 'PARCIAL')
					) > 0 THEN 'Debe dinero'
			WHEN (
					SELECT ISNULL(SUM(ExcesoMonto), 0) 
					FROM dbo.ExcesoPago 
					WHERE AfiliadoId = A.AfiliadoId
					) > 0 THEN 'Tiene dinero a favor'
			ELSE 'Al Dia'
		END AS EstadoGeneral,
		(
			SELECT ISNULL(SUM(MontoPendiente), 0) 
			FROM dbo.Cuotas 
			WHERE AfiliadoId = A.AfiliadoId 
			AND Estado IN ('ADEUDADO', 'PARCIAL')
		) AS 'Monto Adeudado',
		(
			SELECT ISNULL(SUM(ExcesoMonto), 0) 
			FROM dbo.ExcesoPago 
			WHERE AfiliadoId = A.AfiliadoId
		) AS 'Monto a Favor'
	FROM Afiliados A
	WHERE A.AfiliadoId = @AfiliadoId
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_EstadoDeCuentaGeneral]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_EstadoDeCuentaGeneral]
AS
BEGIN
  SELECT 
    a.AfiliadoId AS 'Afiliado', 
    a.Nombre, 
    a.Apellido,
	s.Descripcion AS Suscripcion,
    CASE WHEN SUM(c.Monto) >= 0 THEN 'Al día' ELSE 'Debe' END AS 'Estado de Cuenta',
    MAX(c.Fecha) as 'Fecha del Último Cobro'
  FROM 
    dbo.Afiliados AS a 
    LEFT JOIN dbo.Cobro AS c 
    ON a.AfiliadoId = c.AfiliadoId 
	JOIN dbo.Suscripcion S
	ON A.SuscripcionId = S.SuscripcionId
	WHERE a.Activo = 1
	--AND C.FechaAnulacion IS NULL
  GROUP BY 
    a.AfiliadoId, a.Nombre, a.Apellido, s.Descripcion
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_GenerarCuotas]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GenerarCuotas]
AS
BEGIN
    DECLARE @Hoy DATE;
    SET @Hoy = GETDATE();

    -- Buscamos los afiliados cuyo día de alta coincide con el día actual
    DECLARE AfiliadosCursor CURSOR FOR
    SELECT AfiliadoId, Monto, FechaAlta
    FROM dbo.Afiliados A
    INNER JOIN dbo.Suscripcion S
    ON A.SuscripcionId = S.SuscripcionId
    WHERE DAY(A.FechaAlta) = DAY(@Hoy);

    DECLARE @AfiliadoId INT;
    DECLARE @Monto DECIMAL(10,2);
    DECLARE @FechaAlta DATE;
    DECLARE @MontoDespuesPromocion DECIMAL(10,2);
    DECLARE @ExcesoPago DECIMAL(10,2);

    OPEN AfiliadosCursor;

    FETCH NEXT FROM AfiliadosCursor INTO @AfiliadoId, @Monto, @FechaAlta;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Calculamos el monto después de aplicar la promoción si existe alguna activa para el afiliado
        SET @MontoDespuesPromocion = @Monto - (@Monto * ISNULL((SELECT TOP 1 Descuento FROM dbo.AsignacionPromocionesAfiliados APA INNER JOIN dbo.Promociones P ON APA.PromocionId = P.PromocionId
        WHERE APA.AfiliadoId = @AfiliadoId AND P.Activa = 1 ORDER BY P.FechaCaducidad DESC), 0) / 100);

        -- Obtener el monto excedente (si existe) del afiliado
        SELECT @ExcesoPago = ISNULL(SUM(ExcesoMonto), 0) 
        FROM dbo.ExcesoPago
        WHERE AfiliadoId = @AfiliadoId;

        IF @ExcesoPago >= @MontoDespuesPromocion
        BEGIN
            -- Si el monto excedente cubre la cuota, insertarla como 'SALDADO'
            INSERT INTO dbo.Cuotas (AfiliadoId, Monto, MontoPendiente, FechaCuota, Estado)
            VALUES (@AfiliadoId, @MontoDespuesPromocion, 0, GETDATE(), 'SALDADO');

            -- Actualizar el monto excedente del afiliado
            UPDATE dbo.ExcesoPago
            SET ExcesoMonto = @ExcesoPago - @MontoDespuesPromocion
            WHERE AfiliadoId = @AfiliadoId;
        END
        ELSE IF @ExcesoPago > 0 AND @ExcesoPago < @MontoDespuesPromocion
        BEGIN
            -- Si el monto excedente es menor que la cuota, insertarla como 'PARCIAL'
            INSERT INTO dbo.Cuotas (AfiliadoId, Monto, MontoPendiente, FechaCuota, Estado)
            VALUES (@AfiliadoId, @MontoDespuesPromocion, @MontoDespuesPromocion - @ExcesoPago, GETDATE(), 'PARCIAL');

            -- Actualizar el monto excedente del afiliado a cero
            UPDATE dbo.ExcesoPago
            SET ExcesoMonto = 0
            WHERE AfiliadoId = @AfiliadoId;
        END
        ELSE
        BEGIN
            -- Si no hay monto excedente, insertar la cuota como 'ADEUDADO'
            INSERT INTO dbo.Cuotas (AfiliadoId, Monto, MontoPendiente, FechaCuota, Estado)
            VALUES (@AfiliadoId, @MontoDespuesPromocion, @MontoDespuesPromocion, GETDATE(), 'ADEUDADO');
        END

        FETCH NEXT FROM AfiliadosCursor INTO @AfiliadoId, @Monto, @FechaAlta;
    END;

    CLOSE AfiliadosCursor;
    DEALLOCATE AfiliadosCursor;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_InformeDeudores]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InformeDeudores]
AS
BEGIN
    SELECT 
		A.AfiliadoId,
        A.Nombre, 
        A.Apellido,
        A.DNI,
        A.Telefono,
        C.CuotaId,
        C.FechaCuota,
        C.MontoPendiente AS 'Monto Adeudado'
    FROM 
        dbo.Afiliados A 
    INNER JOIN 
        dbo.Cuotas C ON A.AfiliadoId = C.AfiliadoId
    WHERE 
        C.Estado IN ('ADEUDADO', 'PARCIAL');
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertarCobro]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_InsertarCobro]
    @AfiliadoId INT,
    @Monto DECIMAL(10,2),
    @TipoCobroId INT
AS
BEGIN
    INSERT INTO dbo.Cobro (AfiliadoId, Monto, Fecha, TipoCobroId, FechaAnulacion)
    VALUES (@AfiliadoId, @Monto, GETDATE(), @TipoCobroId, NULL);
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertarPago]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_InsertarPago]
    @Monto DECIMAL(10,2),
    @TipoPagoId INT,
    @UsuarioId INT
AS
BEGIN
    INSERT INTO dbo.Pagos (Monto, Fecha, TipoPagoId, UsuarioId)
    VALUES (@Monto, GETDATE(), @TipoPagoId, @UsuarioId);
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertarSuscripcion]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertarSuscripcion]
    @Descripcion VARCHAR(100),
    @Monto DECIMAL(10,2)
AS
BEGIN
    INSERT INTO dbo.Suscripcion (Descripcion, Monto)
    VALUES (@Descripcion, @Monto);
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertarTipoCobro]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertarTipoCobro]
    @Descripcion VARCHAR(100)
AS
BEGIN
    INSERT INTO dbo.TipoCobro (Descripcion)
    VALUES (@Descripcion);
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertarTipoPago]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InsertarTipoPago]
    @Descripcion VARCHAR(100)
AS
BEGIN
    INSERT INTO dbo.TipoPago (Descripcion)
    VALUES (@Descripcion);
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertarUsuario]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertarUsuario]
    @Nombre VARCHAR(50),
    @Apellido VARCHAR(50),
    @DNI VARCHAR(100),
    @Telefono VARCHAR(100),
    @UserName VARCHAR(100),
    @Password VARCHAR(100),
    @RolId INT
AS
BEGIN
    INSERT INTO dbo.Usuarios (Nombre, Apellido, DNI, Telefono, UserName, Password, Activo, RolId)
    VALUES (@Nombre, @Apellido, @DNI, @Telefono, @UserName, HASHBYTES('SHA2_256', @Password), 1, @RolId);
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_MovimientoHistorico]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_MovimientoHistorico]
    @AfiliadoId INT
AS
BEGIN
    -- Movimientos de cuotas
    SELECT 
        CuotaId,
        FechaCuota,
        Monto,
        MontoPendiente,
        Estado
    FROM dbo.Cuotas
    WHERE AfiliadoId = @AfiliadoId;

    -- Cobros históricos
    SELECT 
        CobroId,
        Monto,
        Fecha,
        TC.Descripcion
    FROM dbo.Cobro C
    INNER JOIN dbo.TipoCobro TC ON C.TipoCobroId = TC.TipoCobroId
    WHERE AfiliadoId = @AfiliadoId
	AND C.FechaAnulacion IS NULL;
    
    -- Promociones históricas
    SELECT 
        P.PromocionId,
        P.Nombre,
        P.Descuento,
        P.FechaCreacion,
        P.FechaCaducidad
    FROM AsignacionPromocionesAfiliados APA
    INNER JOIN Promociones P ON APA.PromocionId = P.PromocionId
    WHERE APA.AfiliadoId = @AfiliadoId;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MovimientoMensual]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_MovimientoMensual]
    @AfiliadoId INT,
    @Year INT,
    @Month INT
AS
BEGIN
    -- Movimientos de cuotas
    SELECT 
        CuotaId,
        FechaCuota,
        Monto,
        MontoPendiente,
        Estado
    FROM dbo.Cuotas
    WHERE AfiliadoId = @AfiliadoId AND YEAR(FechaCuota) = @Year AND MONTH(FechaCuota) = @Month;

    -- Cobros del mes
    SELECT 
        CobroId,
        Monto,
        Fecha,
        TC.Descripcion
    FROM dbo.Cobro C
    INNER JOIN TipoCobro TC ON C.TipoCobroId = TC.TipoCobroId
    WHERE AfiliadoId = @AfiliadoId 
	AND YEAR(Fecha) = @Year 
	AND MONTH(Fecha) = @Month
	AND C.FechaAnulacion IS NULL;
    
    -- Promociones del mes
    SELECT 
        P.PromocionId,
        P.Nombre,
        P.Descuento,
        P.FechaCreacion,
        P.FechaCaducidad
    FROM dbo.AsignacionPromocionesAfiliados APA
    INNER JOIN dbo.Promociones P ON APA.PromocionId = P.PromocionId
    WHERE APA.AfiliadoId = @AfiliadoId AND P.FechaCreacion <= CAST(@Year AS VARCHAR(4)) + '-' + CAST(@Month AS VARCHAR(2)) + '-01' 
    AND P.FechaCaducidad >= DATEADD(MONTH, 1, CAST(@Year AS VARCHAR(4)) + '-' + CAST(@Month AS VARCHAR(2)) + '-01');
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ReporteCajaAnual]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_ReporteCajaAnual]
    @DATE DATE
AS
BEGIN
	DECLARE @Year INT = YEAR(@DATE);
    -- Cobros y Pagos
    SELECT 
        'Cobros' AS TipoMovimiento,
        SUM(Monto) AS Total,
        MONTH(Fecha) AS Mes,
        TC.Descripcion AS Tipo
    FROM dbo.Cobro C
	JOIN dbo.TipoCobro TC
	ON C.TipoCobroId = TC.TipoCobroId
    WHERE YEAR(Fecha) = @Year
	AND C.FechaAnulacion IS NULL
    GROUP BY MONTH(Fecha), TC.Descripcion;

    SELECT 
        'Pagos' AS TipoMovimiento,
        SUM(Monto) AS Total,
        MONTH(Fecha) AS Mes,
        TP.Descripcion AS Tipo
    FROM dbo.Pagos P
	JOIN dbo.TipoPago TP
	ON P.TipoPagoId = TP.TipoPagoId
    WHERE YEAR(Fecha) = @Year
    GROUP BY MONTH(Fecha), TP.Descripcion;

    -- Totales
    SELECT 
        'Total Cobros' AS Descripcion,
        SUM(Monto) AS Monto
    FROM dbo.Cobro
    WHERE YEAR(Fecha) = @Year
	AND FechaAnulacion IS NULL
    UNION
    SELECT 
        'Total Pagos' AS Descripcion,
        SUM(Monto) AS Monto
    FROM dbo.Pagos
    WHERE YEAR(Fecha) = @Year
    UNION
    SELECT 
        'Neto' AS Descripcion,
        ISNULL((SELECT SUM(Monto) FROM dbo.Cobro WHERE YEAR(Fecha) = @Year AND FechaAnulacion IS NULL), 0.00) - ISNULL((SELECT SUM(Monto) FROM dbo.Pagos WHERE YEAR(Fecha) = @Year), 0.00) AS Monto;

    -- Dinero adeudado
    SELECT 
        'Cuotas por cobrar' AS Descripcion,
        SUM(MontoPendiente) AS Monto
    FROM dbo.Cuotas
    WHERE Estado IN ('ADEUDADO', 'PARCIAL');
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_ReporteCajaDiario]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[sp_ReporteCajaDiario]
    @Fecha DATE
AS
BEGIN
    -- Cobros
    SELECT 
        'Cobros' AS TipoMovimiento,
        SUM(Monto) AS Total,
        TC.Descripcion AS Tipo
    FROM dbo.Cobro C
    JOIN dbo.TipoCobro TC
    ON C.TipoCobroId = TC.TipoCobroId
    WHERE CAST(Fecha AS DATE) = @Fecha
	AND C.FechaAnulacion IS NULL
    GROUP BY TC.Descripcion;

    -- Pagos
    SELECT 
        'Pagos' AS TipoMovimiento,
        SUM(Monto) AS Total,
        TP.Descripcion AS Tipo
    FROM dbo.Pagos P
    JOIN dbo.TipoPago TP
    ON P.TipoPagoId = TP.TipoPagoId
    WHERE CAST(Fecha AS DATE) = @Fecha
    GROUP BY TP.Descripcion;

    -- Totales
    SELECT
        'Cobros Totales' AS TipoMovimiento, 
        SUM(Monto) AS Total
    FROM dbo.Cobro
    WHERE CAST(Fecha AS DATE) = @Fecha
	AND FechaAnulacion IS NULL
    UNION ALL
    SELECT
        'Pagos Totales' AS TipoMovimiento, 
        SUM(Monto) AS Total
    FROM dbo.Pagos
    WHERE CAST(Fecha AS DATE) = @Fecha

    UNION ALL
    SELECT
        'Neto Diario' AS TipoMovimiento,
        ISNULL((SELECT SUM(Monto) FROM dbo.Cobro WHERE CAST(Fecha AS DATE) = @Fecha AND FechaAnulacion IS NULL), 0.00) - ISNULL((SELECT SUM(Monto) FROM dbo.Pagos WHERE CAST(Fecha AS DATE) = @Fecha), 0.00) AS NetoTotal
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ReporteCajaMensual]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_ReporteCajaMensual '2023-08-06'
CREATE   PROCEDURE [dbo].[sp_ReporteCajaMensual]
    @DATE DATE
AS
BEGIN
	DECLARE @Year INT = YEAR(@DATE);
	DECLARE @Month INT = MONTH(@DATE);
    -- Cobros
    SELECT 
        'Cobros' AS TipoMovimiento,
        SUM(Monto) AS Total,
        TC.Descripcion AS Tipo
    FROM dbo.Cobro C
    JOIN dbo.TipoCobro TC
    ON C.TipoCobroId = TC.TipoCobroId
    WHERE MONTH(Fecha) = @Month AND YEAR(Fecha) = @Year
	AND C.FechaAnulacion IS NULL
    GROUP BY TC.Descripcion;

    -- Pagos
    SELECT 
        'Pagos' AS TipoMovimiento,
        SUM(Monto) * -1 AS Total,
        TP.Descripcion AS Tipo
    FROM dbo.Pagos P
    JOIN dbo.TipoPago TP
    ON P.TipoPagoId = TP.TipoPagoId
    WHERE MONTH(Fecha) = @Month AND YEAR(Fecha) = @Year
    GROUP BY TP.Descripcion;

    -- Totales
    SELECT
        'Cobros Totales' AS TipoMovimiento, 
        SUM(Monto) AS Total
    FROM dbo.Cobro
    WHERE MONTH(Fecha) = @Month AND YEAR(Fecha) = @Year
    UNION ALL
    SELECT
        'Pagos Totales' AS TipoMovimiento, 
        SUM(Monto) AS Total
    FROM dbo.Pagos
    WHERE MONTH(Fecha) = @Month AND YEAR(Fecha) = @Year

    UNION ALL
    SELECT
        'Neto Mensual' AS TipoMovimiento,
        ISNULL((SELECT SUM(Monto) FROM dbo.Cobro WHERE MONTH(Fecha) = @Month AND YEAR(Fecha) = @Year AND FechaAnulacion IS NULL), 0.00) - ISNULL((SELECT SUM(Monto) FROM dbo.Pagos WHERE MONTH(Fecha) = @Month AND YEAR(Fecha) = @Year), 0.00) AS NetoTotal
END
GO
/****** Object:  StoredProcedure [dbo].[spGenerarReporte]    Script Date: 12/09/2023 13:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[spGenerarReporte]
    @FechaInicio datetime,
    @FechaFin datetime
AS
BEGIN

    -- Listado de Pagos
    SELECT 
        p.Fecha,
		'Pago' AS 'Tipo',
        tp.Descripcion,
        -1 * p.Monto AS Monto -- Convertir a negativo
    FROM dbo.Pagos p
    JOIN dbo.TipoPago tp ON p.TipoPagoId = tp.TipoPagoId
    WHERE p.Fecha BETWEEN @FechaInicio AND @FechaFin

    UNION ALL

    -- Listado de Cobros
    SELECT 
        c.Fecha,
		'Cobro' AS 'Cobro',
        CONCAT(a.Nombre, ' ', a.Apellido) AS NombreAfiliado,
        c.Monto
    FROM dbo.Cobro c
    JOIN dbo.Afiliados a 
	ON c.AfiliadoId = a.AfiliadoId
    WHERE c.Fecha BETWEEN @FechaInicio AND @FechaFin
	AND C.FechaAnulacion IS NULL

    UNION ALL

    -- Listado de Cuotas Por Cobrar
    SELECT 
        cu.FechaCuota,
		'Pendientes de cobro' AS 'Pendientes de cobro',
        CONCAT(a.Nombre, ' ', a.Apellido) AS NombreAfiliado,
        cu.MontoPendiente
    FROM dbo.Cuotas cu
    JOIN dbo.Afiliados a ON cu.AfiliadoId = a.AfiliadoId
    WHERE cu.FechaCuota BETWEEN @FechaInicio AND @FechaFin
	AND cu.Estado IN ('ADEUDADO', 'PARCIAL')

    -- Neto y Totales
	DECLARE @Neto DECIMAL(10,2), @TotalCuotasPendientes DECIMAL(10,2), @TotalPagos DECIMAL(10,2), @TotalCobros DECIMAL(10,2)

	SELECT @TotalCobros = SUM(Monto) 
	FROM dbo.Cobro 
	WHERE Fecha BETWEEN @FechaInicio AND @FechaFin

	SELECT @TotalPagos = SUM(Monto) 
	FROM dbo.Pagos 
	WHERE Fecha BETWEEN @FechaInicio AND @FechaFin

	SET @Neto = @TotalCobros - @TotalPagos

	SELECT @TotalCuotasPendientes = SUM(MontoPendiente) 
	FROM dbo.Cuotas 
	WHERE FechaCuota BETWEEN @FechaInicio AND @FechaFin

	SELECT @Neto AS Neto, @TotalCuotasPendientes AS CuotasPendientes


END
GO
