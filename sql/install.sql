USE w210811_energym
GO

CREATE TABLE dbo.TipoPago(
	TipoPagoId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Descripcion VARCHAR(100) NOT NULL
)
GO

CREATE TABLE dbo.TipoCobro(
	TipoCobroId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Descripcion VARCHAR(100) NOT NULL
)
GO

CREATE TABLE dbo.Suscripcion(
	SuscripcionId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Descripcion VARCHAR(100) NOT NULL,
	Monto DECIMAL(10,2) NOT NULL
)
GO

CREATE TABLE dbo.Permisos(
	PermisoId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Descripcion VARCHAR(100) NOT NULL
)
GO

CREATE TABLE dbo.Roles(
	RolId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Descripcion VARCHAR(100) NOT NULL
)
GO

CREATE TABLE dbo.AsignacionRolesPermisos(
	AsignacionId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	RolId INT NOT NULL,
	PermisoId INT NOT NULL,
	FOREIGN KEY (RolId) REFERENCES dbo.Roles(RolId),
	FOREIGN KEY (PermisoId) REFERENCES dbo.Permisos(PermisoId)
)
GO

CREATE TABLE dbo.Usuarios(
	UsuarioId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Nombre VARCHAR(50) NOT NULL,
	Apellido VARCHAR(50) NOT NULL,
	DNI VARCHAR(100),
	Telefono VARCHAR(100),
	UserName VARCHAR(100) NOT NULL,
	Password VARCHAR(100) NOT NULL,
	Token VARCHAR(200),
	Activo BIT NOT NULL,
	RolId INT NOT NULL,
	FOREIGN KEY (RolId) REFERENCES dbo.Roles(RolId)
)
GO

CREATE TABLE dbo.Pagos(
	PagoId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Monto DECIMAL(10,2) NOT NULL,
	Fecha DATETIME NOT NULL,
	TipoPagoId INT NOT NULL,
	UsuarioId INT NOT NULL,
	FOREIGN KEY (TipoPagoId) REFERENCES dbo.TipoPago(TipoPagoId),
	FOREIGN KEY (UsuarioId) REFERENCES dbo.Usuarios(UsuarioId)
)
GO

CREATE TABLE dbo.Promociones(
	PromocionId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Nombre VARCHAR(50) NOT NULL,
	Descuento INT NOT NULL,
	FechaCaducidad DATE NOT NULL,
	Activa BIT NOT NULL,
	FechaCreacion DATETIME NOT NULL,
	UsuarioId INT NOT NULL,
	FOREIGN KEY (UsuarioId) REFERENCES dbo.Usuarios(UsuarioId)
)
GO

CREATE TABLE dbo.Afiliados(
	AfiliadoId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Nombre VARCHAR(50) NOT NULL,
	Apellido VARCHAR(50) NOT NULL,
	Telefono VARCHAR(100) NOT NULL,
	DNI VARCHAR(100) NOT NULL,
	Email VARCHAR(100) NOT NULL,
	FechaAlta DATETIME NOT NULL,
	Activo BIT NOT NULL,
	SuscripcionId INT NOT NULL,
	UsuarioId INT NOT NULL,
	FOREIGN KEY (SuscripcionId) REFERENCES dbo.Suscripcion(SuscripcionId),
	FOREIGN KEY (UsuarioId) REFERENCES dbo.Usuarios(UsuarioId)
)
GO

CREATE TABLE dbo.AsignacionPromocionesAfiliados(
	PromocionAfiliadoId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	AfiliadoId INT NOT NULL,
	PromocionId INT NOT NULL,
	FOREIGN KEY (AfiliadoId) REFERENCES dbo.Afiliados(AfiliadoId),
	FOREIGN KEY (PromocionId) REFERENCES dbo.Promociones(PromocionId)
)
GO

CREATE TABLE dbo.Cobro(
	CobroId INT NOT NULL PRIMARY KEY IDENTITY (1,1),
	AfiliadoId INT NOT NULL,
	Monto DECIMAL(10,2) NOT NULL,
	Fecha DATETIME NOT NULL,
	TipoCobroId INT NOT NULL,
  FechaAnulacion DATETIME NULL,
	FOREIGN KEY (AfiliadoId) REFERENCES dbo.Afiliados(AfiliadoId),
	FOREIGN KEY (TipoCobroId) REFERENCES dbo.TipoCobro(TipoCobroId)
)
GO

----
CREATE TABLE dbo.Cuotas(
	CuotaId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	AfiliadoId INT NOT NULL,
	Monto DECIMAL(10,2) NOT NULL,
	FechaCuota DATE NOT NULL,
	Estado VARCHAR(50) NOT NULL,
	MontoPendiente DECIMAL(10,2) NOT NULL,
	FOREIGN KEY (AfiliadoId) REFERENCES dbo.Afiliados(AfiliadoId)
)

GO

CREATE TABLE dbo.ExcesoPago(
	ExcesoPagoId INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	AfiliadoId INT NOT NULL,
	ExcesoMonto DECIMAL(10,2) NOT NULL DEFAULT 0,
	FechaExceso DATETIME NOT NULL DEFAULT GETDATE(),
  CobroId INT NULL,
	FOREIGN KEY (AfiliadoId) REFERENCES dbo.Afiliados(AfiliadoId),
  FOREIGN KEY (CobroId) REFERENCES dbo.Cobro(CobroId),
)
GO

CREATE TABLE dbo.CobrosCuotas
(
    CobroId INT NOT NULL,
    CuotaId INT NOT NULL,
    MontoPago DECIMAL(10,2),
    FechaPago DATETIME,
    FechaAnulacion DATETIME,
    FOREIGN KEY (CobroId) REFERENCES dbo.Cobro(CobroId),
    FOREIGN KEY (CuotaId) REFERENCES dbo.Cuotas(CuotaId),
)


CREATE TABLE dbo.CobrosCuotasAnulados
(
    CobrosCuotasAnuladosId INT IDENTITY(1,1) PRIMARY KEY,
    CobroId INT,
    CuotaId INT,
    MontoPago DECIMAL(10,2),
    FechaPago DATETIME,
    FechaAnulacion DATETIME,
    FOREIGN KEY (CobroId) REFERENCES dbo.Cobro(CobroId),
    FOREIGN KEY (CuotaId) REFERENCES dbo.Cuotas(CuotaId),
)
