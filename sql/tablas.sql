USE w210811_energym
GO
CREATE SCHEMA dbo
GO
/****** Object:  Table [dbo].[Afiliados]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Afiliados](
	[AfiliadoId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Telefono] [varchar](100) NOT NULL,
	[DNI] [varchar](100) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
	[Activo] [bit] NOT NULL,
	[SuscripcionId] [int] NOT NULL,
	[UsuarioId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AfiliadoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AsignacionPromocionesAfiliados]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AsignacionPromocionesAfiliados](
	[PromocionAfiliadoId] [int] IDENTITY(1,1) NOT NULL,
	[AfiliadoId] [int] NOT NULL,
	[PromocionId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PromocionAfiliadoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AsignacionRolesPermisos]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AsignacionRolesPermisos](
	[AsignacionId] [int] IDENTITY(1,1) NOT NULL,
	[RolId] [int] NOT NULL,
	[PermisoId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AsignacionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cobro]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cobro](
	[CobroId] [int] IDENTITY(1,1) NOT NULL,
	[AfiliadoId] [int] NOT NULL,
	[Monto] [decimal](10, 2) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[TipoCobroId] [int] NOT NULL,
	[FechaAnulacion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CobroId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CobrosCuotas]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CobrosCuotas](
	[CobroId] [int] NOT NULL,
	[CuotaId] [int] NOT NULL,
	[MontoPago] [decimal](10, 2) NULL,
	[FechaPago] [datetime] NULL,
	[FechaAnulacion] [datetime] NULL,
 CONSTRAINT [PK_CobrosCuotas] PRIMARY KEY CLUSTERED 
(
	[CobroId] ASC,
	[CuotaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CobrosCuotasAnulados]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CobrosCuotasAnulados](
	[CobrosCuotasAnuladosId] [int] IDENTITY(1,1) NOT NULL,
	[CobroId] [int] NULL,
	[CuotaId] [int] NULL,
	[MontoPago] [decimal](10, 2) NULL,
	[FechaPago] [datetime] NULL,
	[FechaAnulacion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CobrosCuotasAnuladosId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cuotas]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cuotas](
	[CuotaId] [int] IDENTITY(1,1) NOT NULL,
	[AfiliadoId] [int] NOT NULL,
	[Monto] [decimal](10, 2) NOT NULL,
	[FechaCuota] [date] NOT NULL,
	[Estado] [varchar](50) NOT NULL,
	[MontoPendiente] [decimal](10, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CuotaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExcesoPago]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExcesoPago](
	[ExcesoPagoId] [int] IDENTITY(1,1) NOT NULL,
	[AfiliadoId] [int] NOT NULL,
	[ExcesoMonto] [decimal](10, 2) NOT NULL,
	[FechaExceso] [datetime] NOT NULL,
	[CobroId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ExcesoPagoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pagos]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pagos](
	[PagoId] [int] IDENTITY(1,1) NOT NULL,
	[Monto] [decimal](10, 2) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[TipoPagoId] [int] NOT NULL,
	[UsuarioId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PagoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permisos]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permisos](
	[PermisoId] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PermisoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Promociones]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Promociones](
	[PromocionId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descuento] [int] NOT NULL,
	[FechaCaducidad] [date] NOT NULL,
	[Activa] [bit] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[UsuarioId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PromocionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RolId] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Suscripcion]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suscripcion](
	[SuscripcionId] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Monto] [decimal](10, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SuscripcionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoCobro]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoCobro](
	[TipoCobroId] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoCobroId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoPago]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoPago](
	[TipoPagoId] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TipoPagoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 12/09/2023 10:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[DNI] [varchar](100) NULL,
	[Telefono] [varchar](100) NULL,
	[UserName] [varchar](100) NOT NULL,
	[Password] [varchar](100) NOT NULL,
	[Token] [varchar](200) NULL,
	[Activo] [bit] NOT NULL,
	[RolId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExcesoPago] ADD  DEFAULT ((0)) FOR [ExcesoMonto]
GO
ALTER TABLE [dbo].[ExcesoPago] ADD  DEFAULT (getdate()) FOR [FechaExceso]
GO
ALTER TABLE [dbo].[Afiliados]  WITH CHECK ADD FOREIGN KEY([SuscripcionId])
REFERENCES [dbo].[Suscripcion] ([SuscripcionId])
GO
ALTER TABLE [dbo].[Afiliados]  WITH CHECK ADD FOREIGN KEY([UsuarioId])
REFERENCES [dbo].[Usuarios] ([UsuarioId])
GO
ALTER TABLE [dbo].[AsignacionPromocionesAfiliados]  WITH CHECK ADD FOREIGN KEY([AfiliadoId])
REFERENCES [dbo].[Afiliados] ([AfiliadoId])
GO
ALTER TABLE [dbo].[AsignacionPromocionesAfiliados]  WITH CHECK ADD FOREIGN KEY([PromocionId])
REFERENCES [dbo].[Promociones] ([PromocionId])
GO
ALTER TABLE [dbo].[AsignacionRolesPermisos]  WITH CHECK ADD FOREIGN KEY([PermisoId])
REFERENCES [dbo].[Permisos] ([PermisoId])
GO
ALTER TABLE [dbo].[AsignacionRolesPermisos]  WITH CHECK ADD FOREIGN KEY([RolId])
REFERENCES [dbo].[Roles] ([RolId])
GO
ALTER TABLE [dbo].[Cobro]  WITH CHECK ADD FOREIGN KEY([AfiliadoId])
REFERENCES [dbo].[Afiliados] ([AfiliadoId])
GO
ALTER TABLE [dbo].[Cobro]  WITH CHECK ADD FOREIGN KEY([TipoCobroId])
REFERENCES [dbo].[TipoCobro] ([TipoCobroId])
GO
ALTER TABLE [dbo].[CobrosCuotas]  WITH CHECK ADD  CONSTRAINT [FK_CobrosCuotas_Cobro] FOREIGN KEY([CobroId])
REFERENCES [dbo].[Cobro] ([CobroId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CobrosCuotas] CHECK CONSTRAINT [FK_CobrosCuotas_Cobro]
GO
ALTER TABLE [dbo].[CobrosCuotas]  WITH CHECK ADD  CONSTRAINT [FK_CobrosCuotas_Cuota] FOREIGN KEY([CuotaId])
REFERENCES [dbo].[Cuotas] ([CuotaId])
GO
ALTER TABLE [dbo].[CobrosCuotas] CHECK CONSTRAINT [FK_CobrosCuotas_Cuota]
GO
ALTER TABLE [dbo].[CobrosCuotasAnulados]  WITH CHECK ADD  CONSTRAINT [FK_CobrosCuotasAnulados_Cobro] FOREIGN KEY([CobroId])
REFERENCES [dbo].[Cobro] ([CobroId])
GO
ALTER TABLE [dbo].[CobrosCuotasAnulados] CHECK CONSTRAINT [FK_CobrosCuotasAnulados_Cobro]
GO
ALTER TABLE [dbo].[CobrosCuotasAnulados]  WITH CHECK ADD  CONSTRAINT [FK_CobrosCuotasAnulados_Cuota] FOREIGN KEY([CuotaId])
REFERENCES [dbo].[Cuotas] ([CuotaId])
GO
ALTER TABLE [dbo].[CobrosCuotasAnulados] CHECK CONSTRAINT [FK_CobrosCuotasAnulados_Cuota]
GO
ALTER TABLE [dbo].[Cuotas]  WITH CHECK ADD FOREIGN KEY([AfiliadoId])
REFERENCES [dbo].[Afiliados] ([AfiliadoId])
GO
ALTER TABLE [dbo].[ExcesoPago]  WITH CHECK ADD FOREIGN KEY([AfiliadoId])
REFERENCES [dbo].[Afiliados] ([AfiliadoId])
GO
ALTER TABLE [dbo].[ExcesoPago]  WITH CHECK ADD  CONSTRAINT [FK_ExcesoPago_Cobro] FOREIGN KEY([CobroId])
REFERENCES [dbo].[Cobro] ([CobroId])
GO
ALTER TABLE [dbo].[ExcesoPago] CHECK CONSTRAINT [FK_ExcesoPago_Cobro]
GO
ALTER TABLE [dbo].[Pagos]  WITH CHECK ADD FOREIGN KEY([TipoPagoId])
REFERENCES [dbo].[TipoPago] ([TipoPagoId])
GO
ALTER TABLE [dbo].[Pagos]  WITH CHECK ADD FOREIGN KEY([UsuarioId])
REFERENCES [dbo].[Usuarios] ([UsuarioId])
GO
ALTER TABLE [dbo].[Promociones]  WITH CHECK ADD FOREIGN KEY([UsuarioId])
REFERENCES [dbo].[Usuarios] ([UsuarioId])
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD FOREIGN KEY([RolId])
REFERENCES [dbo].[Roles] ([RolId])
GO
