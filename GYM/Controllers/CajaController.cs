﻿using GYM.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace GYM.Controllers
{
    public class CajaController : Controller
    {

        private readonly GymContext _context;

        public CajaController(GymContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(FechaRangoViewModel rango)
        {
            if (rango.FechaInicio == default(DateTime))
            {
                rango.FechaInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }

            if (rango.FechaFin == default(DateTime))
            {
                rango.FechaFin = rango.FechaInicio.AddMonths(1).AddDays(-1);
            }



            //var fechaActual = DateTime.Now;
            //var primerDiaDelMes = new DateTime(fechaActual.Year, fechaActual.Month, 1);
            //var ultimoDiaDelMes = primerDiaDelMes.AddMonths(1).AddDays(-1);

            var connection = _context.Database.GetDbConnection();
            await connection.OpenAsync();

            using var command = connection.CreateCommand();
            command.CommandText = "EXEC spGenerarReporte  @FechaInicio, @FechaFin";
            command.Parameters.Add(new SqlParameter("@FechaInicio", rango.FechaInicio));
            command.Parameters.Add(new SqlParameter("@FechaFin", rango.FechaFin));
            var movimientosCaja = new List<ReporteMovimientos>();
            var movimientosNetos = new List<ReporteNetos>();

            using var reader = await command.ExecuteReaderAsync();
            while (reader.Read())
            {
                movimientosCaja.Add(new ReporteMovimientos
                {
                    Fecha = reader.GetDateTime(0),
                    Tipo = reader.GetString(1),
                    Descripcion = reader.GetString(2),
                    Monto = reader.GetDecimal(3)
                });
            }

            await reader.NextResultAsync();

            while (reader.Read())
            {
                movimientosNetos.Add(new ReporteNetos
                {
                    Neto = reader.IsDBNull(0) ? 0 : reader.GetDecimal(0),
                    CuotasPendientes = reader.IsDBNull(1) ? 0 : reader.GetDecimal(1),
                });
            }

            var modelo = new ReporteCajaViewModel
            {
                ReporteMovimientos = movimientosCaja,
                ReporteNetos = movimientosNetos,
            };

            var tiposUnicos = movimientosCaja.Select(m => m.Tipo).Distinct().ToList();
            ViewBag.Tipos = tiposUnicos;

            var totalesPorTipo = new Dictionary<string, decimal>();
            foreach (var tipo in tiposUnicos)
            {
                var total = movimientosCaja.Where(m => m.Tipo == tipo).Sum(m => m.Monto);
                totalesPorTipo[tipo] = total;
            }
            ViewBag.TotalesPorTipo = totalesPorTipo;

            return View(modelo);
        }



    }
}
