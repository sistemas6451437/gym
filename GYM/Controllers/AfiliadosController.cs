﻿using GYM.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Data;
using System.Diagnostics;

namespace GYM.Controllers
{
    public class AfiliadosController : Controller
    {
        private readonly GymContext _context;

        public AfiliadosController(GymContext context)
        {
            _context = context;
        }

        // GET: AfiliadosController
        public async Task<IActionResult> Index()
        {

            await _context.Database.ExecuteSqlRawAsync("EXECUTE dbo.sp_EstadoDeCuentaGeneral");

            List<AfiliadoViewModel> afiliados = new List<AfiliadoViewModel>();

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "dbo.sp_EstadoDeCuentaGeneral";
                command.CommandType = CommandType.StoredProcedure;
                _context.Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        afiliados.Add(new AfiliadoViewModel
                        {
                            AfiliadoId = Convert.ToInt32(result["Afiliado"]),
                            NombreCompleto = $"{result["Nombre"].ToString()} {result["Apellido"].ToString()}",
                            Suscripción = result["Suscripcion"].ToString(),
                            EstadoDeCuenta = result["Estado de Cuenta"].ToString(),
                            FechaUltimoCobro = result.IsDBNull("Fecha del Último Cobro")
                                            ? (DateTime?)null
                                            : result.GetDateTime("Fecha del Último Cobro")
                        });
                    }
                }
            }

            return View(afiliados);
        }


        // GET: AfiliadosController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            // Carga la información del afiliado y su suscripción relacionada
            var afiliado = await _context.Afiliados
                                    .Include(a => a.Suscripcion) // Esto asegura que EF Core cargue la suscripción relacionada
                                    .FirstOrDefaultAsync(a => a.AfiliadoId == id);

            // Si el afiliado no existe, devuelve un error 404
            if (afiliado == null)
            {
                return NotFound();
            }

            var connection = _context.Database.GetDbConnection();
            await connection.OpenAsync();

            using var commandEstadoDeCuenta = connection.CreateCommand();
            commandEstadoDeCuenta.CommandText = "EXEC sp_EstadoDeCuenta @AfiliadoId";
            commandEstadoDeCuenta.Parameters.Add(new SqlParameter("@AfiliadoId", id));
            EstadoDeCuenta estadoDeCuenta = new EstadoDeCuenta();

            using var readerEstadoDeCuenta = await commandEstadoDeCuenta.ExecuteReaderAsync();
            if (readerEstadoDeCuenta.HasRows)
            {
                while (readerEstadoDeCuenta.Read())
                {
                  
                    estadoDeCuenta = new EstadoDeCuenta
                    {
                        AfiliadoId = readerEstadoDeCuenta.GetInt32(0),
                        Nombre = readerEstadoDeCuenta.GetString(1),
                        Apellido = readerEstadoDeCuenta.GetString(2),
                        EstadoGeneral = readerEstadoDeCuenta.GetString(3),
                        MontoAdeudado = readerEstadoDeCuenta.IsDBNull(4) ? 0 : readerEstadoDeCuenta.GetDecimal(4),
                        MontoAFavor = readerEstadoDeCuenta.IsDBNull(5) ? 0 : readerEstadoDeCuenta.GetDecimal(5)
                    };
                }
            }
            readerEstadoDeCuenta.Close();



            using var command = connection.CreateCommand();
            command.CommandText = "EXEC sp_MovimientoHistorico @AfiliadoId";
            command.Parameters.Add(new SqlParameter("@AfiliadoId", id));

            var movimientosHistoricos = new List<MovimientoHistorico>();
            var cobrosHistoricos = new List<CobroHistorico>();
            var promocionesHistoricas = new List<PromocionHistorico>();

            using var reader = await command.ExecuteReaderAsync();

            while (reader.Read())
            {
                movimientosHistoricos.Add(new MovimientoHistorico
                {
                    CuotaId = reader.GetInt32(0),
                    FechaCuota = reader.GetDateTime(1),
                    Monto = reader.GetDecimal(2),
                    MontoPendiente = reader.GetDecimal(3),
                    Estado = reader.GetString(4)
                });
            }

            await reader.NextResultAsync();

            while (reader.Read())
            {
                cobrosHistoricos.Add(new CobroHistorico
                {
                    CobroId = reader.GetInt32(0),
                    Monto = reader.GetDecimal(1),
                    Fecha = reader.GetDateTime(2),
                    Descripcion = reader.GetString(3)
                });
            }

            await reader.NextResultAsync();

            while (reader.Read())
            {
                promocionesHistoricas.Add(new PromocionHistorico
                {
                    PromocionId = reader.GetInt32(0),
                    Nombre = reader.GetString(1),
                    Descuento = reader.GetInt32(2),
                    FechaCreacion = reader.GetDateTime(3),
                    FechaCaducidad = reader.GetDateTime(4)
                });
            }

            reader.Close();


            var modelo = new AfiliadoDetallesViewModel
            {
                Afiliado = afiliado,
                EstadoDeCuenta = estadoDeCuenta,
                MovimientosHistoricos = movimientosHistoricos,
                CobrosHistoricos = cobrosHistoricos,
                PromocionesHistoricas = promocionesHistoricas
            };

            return View(modelo);
        }


        // GET: Afiliados/Create
        public IActionResult Create()
        {
            ViewBag.Suscripciones = _context.Suscripcions.ToList();
            List<Promocione> promociones = _context.Promociones.ToList();
            promociones.Insert(0, new Promocione { PromocionId = -1, Nombre = "Ninguna" });
            ViewBag.Promociones = promociones;

            return View();
        }

        // POST: Afiliados/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AfiliadoCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@Nombre", model.Nombre),
                    new SqlParameter("@Apellido", model.Apellido),
                    new SqlParameter("@Telefono", model.Telefono),
                    new SqlParameter("@DNI", model.DNI),
                    new SqlParameter("@Email", model.Email),
                    new SqlParameter("@SuscripcionId", model.SuscripcionId),
                    new SqlParameter("@UsuarioId", model.UsuarioId),
                    new SqlParameter("@PromocionId", model.PromocionId == -1 ? (object)DBNull.Value : model.PromocionId)
                };
                if (model.PromocionId == -1)
                {
                    await _context.Database.ExecuteSqlRawAsync("EXECUTE sp_CrearAfiliadoYGenerarCuota @Nombre, @Apellido, @Telefono, @DNI, @Email, @SuscripcionId, @UsuarioId", parameters);
                }
                else
                {
                    await _context.Database.ExecuteSqlRawAsync("EXECUTE sp_CrearAfiliadoYGenerarCuota @Nombre, @Apellido, @Telefono, @DNI, @Email, @SuscripcionId, @UsuarioId, @PromocionId", parameters);
                }
                
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: AfiliadosController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var afiliado = await _context.Afiliados.FindAsync(id);
            if (afiliado == null)
            {
                return NotFound();
            }
            return View(afiliado);
        }

        // POST: AfiliadosController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AfiliadoId,Nombre,Apellido,Telefono,Dni,Email,FechaAlta,Activo,SuscripcionId,UsuarioId")] Afiliado afiliado)
        {
            if (id != afiliado.AfiliadoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(afiliado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AfiliadoExists(afiliado.AfiliadoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(afiliado);
        }

        private bool AfiliadoExists(int id)
        {
            return _context.Afiliados.Any(e => e.AfiliadoId == id);
        }

        // GET: AfiliadosController/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var afiliado = await _context.Afiliados
                .FirstOrDefaultAsync(m => m.AfiliadoId == id);

            if (afiliado == null)
            {
                return NotFound();
            }

            return View(afiliado);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var parameters = new SqlParameter[]
                {
            new SqlParameter("@AfiliadoId", id)
                };
                await _context.Database.ExecuteSqlRawAsync("EXEC sp_DesactivarAfiliado @AfiliadoId", parameters);
            }
            catch (Exception ex)
            {
                // Añade el manejo de excepciones que necesites aquí, 
                // posiblemente querrás devolver un mensaje de error al usuario.
                return View("Error", new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier, ErrorMessage = ex.Message });
            }

            return RedirectToAction(nameof(Index));
        }

    }
}
