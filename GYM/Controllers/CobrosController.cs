﻿using GYM.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace GYM.Controllers
{
    public class CobrosController : Controller
    {
        private readonly GymContext _context;

        public CobrosController(GymContext context)
        {
            _context = context;
        }

        // GET: CobrosController
        public async Task<IActionResult> Index()
        {
            var cobros = await _context.Cobros
                                    .Where(c => c.FechaAnulacion == null)
                                    .Include(a => a.Afiliado) // Esto asegura que EF Core cargue la suscripción relacionada
                                    .Include(b => b.TipoCobro)
                                    .OrderByDescending(c => c.Fecha)
                                    .ToListAsync();

            return View(cobros);
        }

        // GET: CobrosController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CobrosController/Create
        public async Task<IActionResult> Create()
        {
            var model = new CobroCreateViewModel
            {
                Afiliados = new SelectList(await _context.Afiliados.ToListAsync(), "AfiliadoId", "NombreCompleto"),
                TiposCobro = new SelectList(await _context.TipoCobros.ToListAsync(), "TipoCobroId", "Descripcion")
            };

            return View(model);
        }

        // POST: CobrosController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AfiliadoId,Monto,TipoCobroId")] Cobro cobro)
        {
            if(cobro.AfiliadoId == 0)
            {
                TempData["Message"] = "Falta ingresar el afiliado.";
                TempData["MessageType"] = "Error";
            }
            else if (cobro.Monto == 0)
            {
                TempData["Message"] = "Falta ingresar el monto";
                TempData["MessageType"] = "Error";
            }
            else if (cobro.TipoCobroId == 0)
            {
                TempData["Message"] = "Falta ingresar el tipo de cobro";
                TempData["MessageType"] = "Error";
            }
            else
            {
                var parameters = new[]
                {
                    new SqlParameter("@AfiliadoId", cobro.AfiliadoId),
                    new SqlParameter("@Monto", cobro.Monto),
                    new SqlParameter("@TipoCobroId", cobro.TipoCobroId)
                };

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_InsertarCobro @AfiliadoId, @Monto, @TipoCobroId", parameters);
                TempData["Message"] = "El cobro se realizó con éxito.";
                TempData["MessageType"] = "Exito";
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Index));
        }


        // GET: CobrosController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CobrosController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CobrosController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CobrosController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var parameters = new SqlParameter("@CobroId", id);

            try
            {
                await _context.Database.ExecuteSqlRawAsync("EXEC sp_AnularCobro @CobroId", parameters);
                return Json(new { success = true, message = "El cobro fue anulado con éxito.", messageType = "Exito" });
            }
            catch (DbUpdateException ex)
            {
                return Json(new { success = false, message = ex.InnerException?.Message ?? ex.Message, messageType = "Error" });
            }
            catch (SqlException ex)
            {
                return Json(new { success = false, message = ex.InnerException?.Message ?? ex.Message, messageType = "Error" });
            }
            catch (Exception ex) // Para atrapar cualquier otro tipo de excepción
            {
                return Json(new { success = false, message = ex.InnerException?.Message ?? ex.Message, messageType = "Error" });
            }

        }
    }
}
