﻿using GYM.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace GYM.Controllers
{
    public class PagosController : Controller
    {
        private readonly GymContext _context;

        public PagosController(GymContext context)
        {
            _context = context;
        }
        // GET: PagosController
        public async Task<IActionResult> Index()
        {
            // Trae la lista de pagos junto con su TipoPago
            var pagos = await _context.Pagos.Include(p => p.TipoPago).ToListAsync();

            return View(pagos);
        }

        // GET: PagosController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PagosController/Create
        public IActionResult Create()
        {
            // Lista de tipos de pago para el dropdown
            var tipoPagos = _context.TipoPagos.ToList();
            ViewBag.TipoPagos = new SelectList(tipoPagos, "TipoPagoId", "Descripcion");
            return View();
        }

        // POST: PagosController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TipoPagoId,Monto")] Pago pago)
        {
            

            if (pago.TipoPagoId != null && pago.Monto != null)
            {
                using var connection = _context.Database.GetDbConnection();
                await connection.OpenAsync();

                using var command = connection.CreateCommand();
                command.CommandText = "EXEC sp_InsertarPago @Monto, @TipoPagoId, @UsuarioId";
                command.Parameters.AddRange(new[]
                {
                    new SqlParameter("@Monto", pago.Monto),
                    new SqlParameter("@TipoPagoId", pago.TipoPagoId),
                    new SqlParameter("@UsuarioId", 1) // UsuarioId siempre es 1 según lo indicado
                });

                await command.ExecuteNonQueryAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(pago);
        }

        // GET: PagosController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PagosController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PagosController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PagosController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
