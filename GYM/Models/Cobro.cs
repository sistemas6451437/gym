﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Cobro
{
    public int CobroId { get; set; }

    public int AfiliadoId { get; set; }

    public decimal Monto { get; set; }

    public DateTime Fecha { get; set; }

    public int TipoCobroId { get; set; }

    public DateTime? FechaAnulacion { get; set; }

    public virtual Afiliado Afiliado { get; set; } = null!;

    public virtual TipoCobro TipoCobro { get; set; } = null!;

}
