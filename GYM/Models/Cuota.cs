﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Cuota
{
    public int CuotaId { get; set; }

    public int AfiliadoId { get; set; }

    public decimal Monto { get; set; }

    public DateTime FechaCuota { get; set; }

    public string Estado { get; set; } = null!;

    public decimal MontoPendiente { get; set; }

    public virtual Afiliado Afiliado { get; set; } = null!;
}
