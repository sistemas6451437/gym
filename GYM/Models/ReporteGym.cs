﻿using Microsoft.EntityFrameworkCore;

namespace GYM.Models
{
    public class ReporteGym
    {
        public DateTime? Fecha { get; set; }
        public string? Descripcion { get; set; } // Esto es para Pago, Cobro, o Pendientes de cobro
        public string? NombreAfiliado { get; set; }
        public decimal? Monto { get; set; }
        public decimal? Neto { get; set; }
        public decimal? CuotasPendientes { get; set; }

    }
}
