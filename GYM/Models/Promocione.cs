﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Promocione
{
    public int PromocionId { get; set; }

    public string Nombre { get; set; } = null!;

    public int Descuento { get; set; }

    public DateTime FechaCaducidad { get; set; }

    public bool Activa { get; set; }

    public DateTime FechaCreacion { get; set; }

    public int UsuarioId { get; set; }

    public virtual ICollection<AsignacionPromocionesAfiliado> AsignacionPromocionesAfiliados { get; set; } = new List<AsignacionPromocionesAfiliado>();

    public virtual Usuario Usuario { get; set; } = null!;
}
