﻿namespace GYM.Models
{
    public class AfiliadoDetallesViewModel
    {
        public Afiliado Afiliado { get; set; }  // Esto representa los detalles básicos del afiliado.
        public EstadoDeCuenta EstadoDeCuenta { get; set; } // Esto representa el estado de cuenta del afiliado.
        public List<MovimientoHistorico> MovimientosHistoricos { get; set; } // Esto representa los movimientos históricos del afiliado.
        public List<CobroHistorico> CobrosHistoricos { get; set; } // Esto representa los cobros históricos del afiliado.
        public List<PromocionHistorico> PromocionesHistoricas { get; set; } // Esto representa las promociones históricas del afiliado.
    }

}
