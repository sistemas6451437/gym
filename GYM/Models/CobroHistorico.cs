﻿namespace GYM.Models
{
    public class CobroHistorico
    {
        public int CobroId { get; set; }
        public decimal Monto { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
    }
}
