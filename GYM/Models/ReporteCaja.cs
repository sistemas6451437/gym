﻿namespace GYM.Models
{
    public class ReporteCaja
    {
        public string TipoMovimiento { get; set; }
        public decimal Total { get; set; }
        public string? Tipo { get; set; } // Puede ser null
    }
}
