﻿namespace GYM.Models
{
    public class ReporteMovimientos
    {
        public DateTime Fecha { get; set; }
        public string Tipo { get; set; } // Esto es para Pago, Cobro, o Pendientes de cobro
        public string Descripcion { get; set; }
        public decimal Monto { get; set; }
    }
}