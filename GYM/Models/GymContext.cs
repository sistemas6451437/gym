﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using GYM.Models;

namespace GYM.Models;

public partial class GymContext : DbContext
{
    public GymContext()
    {
    }

    public GymContext(DbContextOptions<GymContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Afiliado> Afiliados { get; set; }

    public virtual DbSet<AsignacionPromocionesAfiliado> AsignacionPromocionesAfiliados { get; set; }

    public virtual DbSet<AsignacionRolesPermiso> AsignacionRolesPermisos { get; set; }

    public virtual DbSet<Cobro> Cobros { get; set; }

    public virtual DbSet<Cuota> Cuotas { get; set; }

    public virtual DbSet<ExcesoPago> ExcesoPagos { get; set; }

    public virtual DbSet<Pago> Pagos { get; set; }

    public virtual DbSet<Permiso> Permisos { get; set; }

    public virtual DbSet<Promocione> Promociones { get; set; }

    public virtual DbSet<Role> Roles { get; set; }

    public virtual DbSet<Suscripcion> Suscripcions { get; set; }

    public virtual DbSet<TipoCobro> TipoCobros { get; set; }

    public virtual DbSet<TipoPago> TipoPagos { get; set; }

    public virtual DbSet<Usuario> Usuarios { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=LAPTOP-S1NK3IMD;Database=GYM;Trusted_Connection=True;Encrypt=False;TrustServerCertificate=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Afiliado>(entity =>
        {
            entity.HasKey(e => e.AfiliadoId).HasName("PK__Afiliado__C44A3AEA6C447B0E");

            entity.Property(e => e.Apellido)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Dni)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("DNI");
            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.FechaAlta).HasColumnType("datetime");
            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Telefono)
                .HasMaxLength(100)
                .IsUnicode(false);

            entity.HasOne(d => d.Suscripcion).WithMany(p => p.Afiliados)
                .HasForeignKey(d => d.SuscripcionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Afiliados__Suscr__4E88ABD4");

            entity.HasOne(d => d.Usuario).WithMany(p => p.Afiliados)
                .HasForeignKey(d => d.UsuarioId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Afiliados__Usuar__4F7CD00D");
        });

        modelBuilder.Entity<AsignacionPromocionesAfiliado>(entity =>
        {
            entity.HasKey(e => e.PromocionAfiliadoId).HasName("PK__Asignaci__3C0C74BAF2530B95");

            entity.HasOne(d => d.Afiliado).WithMany(p => p.AsignacionPromocionesAfiliados)
                .HasForeignKey(d => d.AfiliadoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Asignacio__Afili__52593CB8");

            entity.HasOne(d => d.Promocion).WithMany(p => p.AsignacionPromocionesAfiliados)
                .HasForeignKey(d => d.PromocionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Asignacio__Promo__534D60F1");
        });

        modelBuilder.Entity<AsignacionRolesPermiso>(entity =>
        {
            entity.HasKey(e => e.AsignacionId).HasName("PK__Asignaci__D82B5B57C4CD8706");

            entity.HasOne(d => d.Permiso).WithMany(p => p.AsignacionRolesPermisos)
                .HasForeignKey(d => d.PermisoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Asignacio__Permi__4222D4EF");

            entity.HasOne(d => d.Rol).WithMany(p => p.AsignacionRolesPermisos)
                .HasForeignKey(d => d.RolId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Asignacio__RolId__412EB0B6");
        });

        modelBuilder.Entity<Cobro>(entity =>
        {
            entity.HasKey(e => e.CobroId).HasName("PK__Cobro__6222706CF86F2132");

            entity.ToTable("Cobro", tb => tb.HasTrigger("tr_Cobros_INSERT"));

            entity.Property(e => e.Fecha).HasColumnType("datetime");
            entity.Property(e => e.Monto).HasColumnType("decimal(10, 2)");

            entity.HasOne(d => d.Afiliado).WithMany(p => p.Cobros)
                .HasForeignKey(d => d.AfiliadoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Cobro__AfiliadoI__5629CD9C");

            entity.HasOne(d => d.TipoCobro).WithMany(p => p.Cobros)
                .HasForeignKey(d => d.TipoCobroId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Cobro__TipoCobro__571DF1D5");
        });

        modelBuilder.Entity<Cuota>(entity =>
        {
            entity.HasKey(e => e.CuotaId).HasName("PK__Cuotas__319DB38198115445");

            entity.Property(e => e.Estado)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.FechaCuota).HasColumnType("date");
            entity.Property(e => e.Monto).HasColumnType("decimal(10, 2)");
            entity.Property(e => e.MontoPendiente).HasColumnType("decimal(10, 2)");

            entity.HasOne(d => d.Afiliado).WithMany(p => p.Cuota)
                .HasForeignKey(d => d.AfiliadoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Cuotas__Afiliado__6477ECF3");
        });

        modelBuilder.Entity<ExcesoPago>(entity =>
        {
            entity.HasKey(e => e.ExcesoPagoId).HasName("PK__ExcesoPa__CFAFC928F213FC61");

            entity.ToTable("ExcesoPago");

            entity.Property(e => e.ExcesoMonto).HasColumnType("decimal(10, 2)");
            entity.Property(e => e.FechaExceso)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");

            entity.HasOne(d => d.Afiliado).WithMany(p => p.ExcesoPagos)
                .HasForeignKey(d => d.AfiliadoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__ExcesoPag__Afili__5EBF139D");
        });

        modelBuilder.Entity<Pago>(entity =>
        {
            entity.HasKey(e => e.PagoId).HasName("PK__Pagos__F00B61382A4E71E4");

            entity.Property(e => e.Fecha).HasColumnType("datetime");
            entity.Property(e => e.Monto).HasColumnType("decimal(10, 2)");

            entity.HasOne(d => d.TipoPago).WithMany(p => p.Pagos)
                .HasForeignKey(d => d.TipoPagoId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Pagos__TipoPagoI__47DBAE45");

            entity.HasOne(d => d.Usuario).WithMany(p => p.Pagos)
                .HasForeignKey(d => d.UsuarioId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Pagos__UsuarioId__48CFD27E");
        });

        modelBuilder.Entity<Permiso>(entity =>
        {
            entity.HasKey(e => e.PermisoId).HasName("PK__Permisos__96E0C723DC456AA5");

            entity.Property(e => e.Descripcion)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Promocione>(entity =>
        {
            entity.HasKey(e => e.PromocionId).HasName("PK__Promocio__2DA61D9DE272FC44");

            entity.Property(e => e.FechaCaducidad).HasColumnType("date");
            entity.Property(e => e.FechaCreacion).HasColumnType("datetime");
            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .IsUnicode(false);

            entity.HasOne(d => d.Usuario).WithMany(p => p.Promociones)
                .HasForeignKey(d => d.UsuarioId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Promocion__Usuar__4BAC3F29");
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.HasKey(e => e.RolId).HasName("PK__Roles__F92302F18F3F619A");

            entity.Property(e => e.Descripcion)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Suscripcion>(entity =>
        {
            entity.HasKey(e => e.SuscripcionId).HasName("PK__Suscripc__814D76AB1AB8E5EA");

            entity.ToTable("Suscripcion");

            entity.Property(e => e.Descripcion)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Monto).HasColumnType("decimal(10, 2)");
        });

        modelBuilder.Entity<TipoCobro>(entity =>
        {
            entity.HasKey(e => e.TipoCobroId).HasName("PK__TipoCobr__E7163D655AE47613");

            entity.ToTable("TipoCobro");

            entity.Property(e => e.Descripcion)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<TipoPago>(entity =>
        {
            entity.HasKey(e => e.TipoPagoId).HasName("PK__TipoPago__424FFE0B667BCEB5");

            entity.ToTable("TipoPago");

            entity.Property(e => e.Descripcion)
                .HasMaxLength(100)
                .IsUnicode(false);
        });

        modelBuilder.Entity<Usuario>(entity =>
        {
            entity.HasKey(e => e.UsuarioId).HasName("PK__Usuarios__2B3DE7B8ADE0042A");

            entity.Property(e => e.Apellido)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Dni)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasColumnName("DNI");
            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .IsUnicode(false);
            entity.Property(e => e.Password)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Telefono)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.Token)
                .HasMaxLength(200)
                .IsUnicode(false);
            entity.Property(e => e.UserName)
                .HasMaxLength(100)
                .IsUnicode(false);

            entity.HasOne(d => d.Rol).WithMany(p => p.Usuarios)
                .HasForeignKey(d => d.RolId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Usuarios__RolId__44FF419A");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

    public DbSet<GYM.Models.AfiliadoViewModel>? AfiliadoViewModel { get; set; }

    public DbSet<GYM.Models.AfiliadoCreateViewModel>? AfiliadoCreateViewModel { get; set; }
}
