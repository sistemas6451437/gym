﻿namespace GYM.Models
{
    public class ReporteCajaDiario
    {
        public List<MovimientoCaja> Cobros { get; set; }
        public List<MovimientoCaja> Pagos { get; set; }
        public List<TotalCaja> Totales { get; set; }
    }
}
