﻿namespace GYM.Models
{
    public class PromocionHistorico
    {
        public int PromocionId { get; set; }
        public string Nombre { get; set; }
        public decimal Descuento { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaCaducidad { get; set; }
    }
}
