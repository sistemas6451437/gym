﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace GYM.Models
{
    public class CobroCreateViewModel
    {
        public int Id { get; set; }
        public Cobro Cobro { get; set; }

        public IEnumerable<SelectListItem> Afiliados { get; set; }
        public IEnumerable<SelectListItem> TiposCobro { get; set; }
    }
}
