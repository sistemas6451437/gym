﻿using MessagePack;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel;

namespace GYM.Models
{
    public class AfiliadoViewModel
    {
        public int Id { get; set; }
        [DisplayName("Nº de Afiliado")]
        public int AfiliadoId { get; set; }
        public string NombreCompleto { get; set; }
        public string Suscripción { get; set; }
        [DisplayName("Estado de Cuenta")]
        public string EstadoDeCuenta { get; set; }
        [DisplayName("Fecha último Cobro")]
        public DateTime? FechaUltimoCobro { get; set; }
    }

}
