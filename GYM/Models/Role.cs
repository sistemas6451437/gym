﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Role
{
    public int RolId { get; set; }

    public string Descripcion { get; set; } = null!;

    public virtual ICollection<AsignacionRolesPermiso> AsignacionRolesPermisos { get; set; } = new List<AsignacionRolesPermiso>();

    public virtual ICollection<Usuario> Usuarios { get; set; } = new List<Usuario>();
}
