﻿namespace GYM.Models
{
    public class FechaRangoViewModel
    {
        public DateTime FechaInicio { get; set; } = DateTime.Now;
        public DateTime FechaFin { get; set; } = DateTime.Now;
    }
}
