﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class ExcesoPago
{
    public int ExcesoPagoId { get; set; }

    public int AfiliadoId { get; set; }

    public decimal ExcesoMonto { get; set; }

    public DateTime FechaExceso { get; set; }

    public virtual Afiliado Afiliado { get; set; } = null!;
}
