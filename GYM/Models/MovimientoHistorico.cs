﻿namespace GYM.Models
{
    public class MovimientoHistorico
    {
        public int CuotaId { get; set; }
        public DateTime FechaCuota { get; set; }
        public decimal Monto { get; set; }
        public decimal MontoPendiente { get; set; }
        public string Estado { get; set; }
    }

}
