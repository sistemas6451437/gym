﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Afiliado
{
    public int AfiliadoId { get; set; }

    public string Nombre { get; set; } = null!;

    public string Apellido { get; set; } = null!;

    public string Telefono { get; set; } = null!;

    public string Dni { get; set; } = null!;

    public string Email { get; set; } = null!;

    public DateTime FechaAlta { get; set; }

    public bool Activo { get; set; }

    public int SuscripcionId { get; set; }

    public int UsuarioId { get; set; }

    public virtual ICollection<AsignacionPromocionesAfiliado> AsignacionPromocionesAfiliados { get; set; } = new List<AsignacionPromocionesAfiliado>();

    public virtual ICollection<Cobro> Cobros { get; set; } = new List<Cobro>();

    public virtual ICollection<Cuota> Cuota { get; set; } = new List<Cuota>();

    public virtual ICollection<ExcesoPago> ExcesoPagos { get; set; } = new List<ExcesoPago>();

    public virtual Suscripcion Suscripcion { get; set; } = null!;

    public virtual Usuario Usuario { get; set; } = null!;
    public string NombreCompleto
    {
        get { return Nombre + " " + Apellido; }
    }


}
