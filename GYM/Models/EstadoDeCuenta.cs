﻿namespace GYM.Models
{
    public class EstadoDeCuenta
    {
        public int AfiliadoId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string EstadoGeneral { get; set; }
        public decimal MontoAdeudado { get; set; }
        public decimal MontoAFavor { get; set; }
    }

}
