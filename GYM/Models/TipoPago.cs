﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class TipoPago
{
    public int TipoPagoId { get; set; }

    public string Descripcion { get; set; } = null!;

    public virtual ICollection<Pago> Pagos { get; set; } = new List<Pago>();
}
