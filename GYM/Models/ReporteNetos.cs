﻿namespace GYM.Models
{
    public class ReporteNetos
    {
        public decimal? Neto { get; set; }
        public decimal? CuotasPendientes { get; set; }
    }
}