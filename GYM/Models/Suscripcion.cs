﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Suscripcion
{
    public int SuscripcionId { get; set; }

    public string Descripcion { get; set; } = null!;

    public decimal Monto { get; set; }

    public virtual ICollection<Afiliado> Afiliados { get; set; } = new List<Afiliado>();
}
