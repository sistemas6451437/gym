﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Usuario
{
    public int UsuarioId { get; set; }

    public string Nombre { get; set; } = null!;

    public string Apellido { get; set; } = null!;

    public string? Dni { get; set; }

    public string? Telefono { get; set; }

    public string UserName { get; set; } = null!;

    public string Password { get; set; } = null!;

    public string? Token { get; set; }

    public bool Activo { get; set; }

    public int RolId { get; set; }

    public virtual ICollection<Afiliado> Afiliados { get; set; } = new List<Afiliado>();

    public virtual ICollection<Pago> Pagos { get; set; } = new List<Pago>();

    public virtual ICollection<Promocione> Promociones { get; set; } = new List<Promocione>();

    public virtual Role Rol { get; set; } = null!;
}
