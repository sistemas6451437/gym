﻿namespace GYM.Models
{
    public class AfiliadoCreateViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string DNI { get; set; }
        public string Email { get; set; }
        public int SuscripcionId { get; set; }
        public int UsuarioId { get; set; }
        public int? PromocionId { get; set; }
    }

}
