﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class AsignacionPromocionesAfiliado
{
    public int PromocionAfiliadoId { get; set; }

    public int AfiliadoId { get; set; }

    public int PromocionId { get; set; }

    public virtual Afiliado Afiliado { get; set; } = null!;

    public virtual Promocione Promocion { get; set; } = null!;
}
