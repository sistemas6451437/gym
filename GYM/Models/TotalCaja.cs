﻿namespace GYM.Models
{
    public class TotalCaja
    {
        public string TipoMovimiento { get; set; }
        public decimal Total { get; set; }
    }
}
