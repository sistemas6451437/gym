﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class TipoCobro
{
    public int TipoCobroId { get; set; }

    public string Descripcion { get; set; } = null!;

    public virtual ICollection<Cobro> Cobros { get; set; } = new List<Cobro>();
}
