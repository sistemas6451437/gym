﻿namespace GYM.Models
{
    public class MovimientoCaja
    {
        public string TipoMovimiento { get; set; }
        public decimal Total { get; set; }
        public string Tipo { get; set; }
    }
}
