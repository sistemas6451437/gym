﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Pago
{
    public int PagoId { get; set; }

    public decimal Monto { get; set; }

    public DateTime Fecha { get; set; }

    public int TipoPagoId { get; set; }

    public int UsuarioId { get; set; }

    public virtual TipoPago TipoPago { get; set; } = null!;

    public virtual Usuario Usuario { get; set; } = null!;
}
