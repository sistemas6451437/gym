﻿using System;
using System.Collections.Generic;

namespace GYM.Models;

public partial class Permiso
{
    public int PermisoId { get; set; }

    public string Descripcion { get; set; } = null!;

    public virtual ICollection<AsignacionRolesPermiso> AsignacionRolesPermisos { get; set; } = new List<AsignacionRolesPermiso>();
}
