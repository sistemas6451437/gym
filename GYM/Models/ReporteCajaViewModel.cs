﻿namespace GYM.Models
{
    public class ReporteCajaViewModel
    {
        public List<ReporteMovimientos> ReporteMovimientos { get; set; }
        public List<ReporteNetos> ReporteNetos { get; set; }

    }
}
